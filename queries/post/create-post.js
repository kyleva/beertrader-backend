const verifyJWT = require('./../../utils/verify-jwt')

const models = require('./../../models')
const Post = models.Post
const Comment = models.Comment

module.exports = (req, res, next) => {
  const { title, description, userId, token } = req.body

  verifyJWT(token, process.env.JWT_SECRET)
    .then(user => {
      Post.create({
        title,
        body: description,
        userId
      })
        .then(post => {
          res.status(200).json({
            id: post.id
          })
        })
        .catch(err => {
          console.log(err)
        })
    })
    .catch(err => {
      res.cookie('token', '', { expires: new Date(0) })
      res.status(200).json({
        error: err.name
      })
    })
}
