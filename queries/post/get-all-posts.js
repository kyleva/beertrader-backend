const models = require('./../../models')
const Post = models.Post
const User = models.User

module.exports = (req, res, next) => {
  Post.findAll({
    order: [['updatedAt', 'DESC']],
    include: [
      {
        model: User,
        as: 'user'
      }
    ]
  }).then(posts => {
    res.status(200).json({
      data: posts
    })
  })
}
