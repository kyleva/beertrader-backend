const models = require('./../../models')
const Post = models.Post
const Comment = models.Comment
const User = models.User

module.exports = (req, res, next) => {
  const id = req.params.postId
  Post.find({
    where: {
      id: id
    },
    include: [
      {
        model: Comment,
        as: 'comments'
      },
      {
        model: User,
        as: 'user'
      }
    ]
  }).then(post => {
    res.status(200).json({
      data: post.dataValues
    })
  })
}
