const models = require('./../../models')
const Comment = models.Comment

// const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  const { body, username, postId, userId } = req.body

  console.log(body, username, postId, userId)

  Comment.create({
    body,
    username: username,
    userId,
    postId,
  })
    .then(comment => {
      res.status(200).json({
        id: comment.dataValues.id,
        body: comment.dataValues.body,
        username: comment.dataValues.username,
      })
    })
    .catch(err => {
      console.log(err)
    })
}
