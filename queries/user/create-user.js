const User = require('./../../models/user')

module.exports = username => {
  return User.findOrCreate({
    where: {
      username
    }
  })
}
