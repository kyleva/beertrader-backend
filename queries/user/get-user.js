const models = require('./../../models')
const User = models.User
const Comment = models.Comment
const Post = models.Post

module.exports = (req, res, next) => {
  const username = req.params.username

  User.find({
    where: {
      username: username
    },
    include: [
      {
        model: Comment,
        as: 'comments'
      },
      {
        model: Post,
        as: 'posts'
      }
    ]
  }).then(user => {
    res.status(200).json({
      data: user
    })
  })
}
