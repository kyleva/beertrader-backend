const jwt = require('jsonwebtoken')

module.exports = token => {
  return new Promise((resolve, reject) => {
    console.log('try and decode')
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      console.log('do something')
      if (err || !decoded) {
        return reject(err)
      } else {
        resolve(decoded.user)
      }
    })
  })
}
