module.exports = (fields, obj) => {
  return fields.reduce((acc, curr) => {
    if (obj[curr]) acc[curr] = obj[curr]
    return acc
  }, {})
}
