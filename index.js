if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', '*')
//   res.header(
//     'Access-Control-Allow-Methods',
//     'GET,PUT,POST,DELETE,PATCH,OPTIONS',
//   )
//   res.header(
//     'Access-Control-Allow-Headers',
//     'Origin, X-Requested-With, Content-Type, Accept',
//   )
//   next()
// })

app.use(cors())
app.options('*', cors())

// configure app
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// load passport strategy
require('./auth/passport').init(app)

// setup routes
const routes = require('./routes')
app.use(routes)

app.set('port', process.env.PORT || 9090)

app.listen(app.get('port'), () => {
  console.log(`App is running on port ${app.get('port')}`)
})
