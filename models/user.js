'use strict'
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define(
    'User',
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      username: DataTypes.STRING,
      verified: DataTypes.BOOLEAN,
    },
    {},
  )
  // DataTypes.STRING
  User.associate = function(models) {
    User.hasMany(models.Post, {
      as: 'posts',
      foreignKey: 'userId',
    })

    User.hasMany(models.Comment, {
      as: 'comments',
      foreignKey: 'userId',
    })
  }

  return User
}
