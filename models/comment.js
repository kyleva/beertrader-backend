'use strict'
module.exports = (sequelize, DataTypes) => {
  var Comment = sequelize.define(
    'Comment',
    {
      body: DataTypes.TEXT,
      username: DataTypes.STRING
    },
    {}
  )

  Comment.associate = function(models) {
    Comment.belongsTo(models.User, {
      as: 'user',
      onDelete: 'CASCADE',
      foreignKey: 'userId'
    })

    Comment.belongsTo(models.Post, {
      as: 'post',
      onDelete: 'CASCADE',
      foreignKey: 'postId'
    })
  }

  return Comment
}
