'use strict'
module.exports = (sequelize, DataTypes) => {
  var Post = sequelize.define(
    'Post',
    {
      title: DataTypes.STRING,
      body: DataTypes.TEXT
    },
    {}
  )

  Post.associate = function(models) {
    Post.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'userId',
      onDelete: 'CASCADE'
    })

    Post.hasMany(models.Comment, {
      as: 'comments',
      foreignKey: 'postId'
    })
  }
  return Post
}
