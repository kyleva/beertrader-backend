const cookieParser = require('cookie-parser')
const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const methodOverride = require('method-override')
const passport = require('passport')
const session = require('express-session')

const loginStrategy = require('./login')
const registerStrategy = require('./register')

const User = require('./../../models').User

const init = app => {
  app.use(cookieParser('glovecat'))
  app.use(methodOverride())
  app.use(
    session({
      cookie: {
        httpOnly: false,
        maxAge: 1000 * 60 * 60,
        secure: false,
      },
      resave: true,
      saveUninitialized: true,
      secret: 'secret',
    }),
  )

  app.use(passport.initialize())
  app.use(passport.session())

  passport.serializeUser((user, done) => {
    done(null, user.id)
  })

  passport.deserializeUser((id, done) => {
    User.findUserById(id).then(user => {
      if (user) done(null, user.get())
      else {
        done(user.errors, null)
      }
    })
  })

  loginStrategy(passport, User)
  registerStrategy(passport, User)
}

module.exports = {
  init,
}
