const passport = require('passport')
const session = require('express-session')
const LocalStrategy = require('passport-local')

var bCrypt = require('bcrypt-nodejs')

module.exports = (passport, User) => {
  passport.use(
    'local-register',
    new LocalStrategy(
      {
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true,
      },
      (req, username, password, done) => {
        const generateHash = password => {
          return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null)
        }
        const email = req.body.email

        User.findOne({
          where: {
            email,
          },
        }).then(user => {
          if (user) {
            done({
              message: 'This email or username is already in use.',
            })
          } else {
            const userPassword = generateHash(password)

            User.create({
              email,
              password: userPassword,
              username,
              verified: true,
            }).then(newUser => {
              if (!newUser) {
                return done({
                  message:
                    "Something went wrong .. we're not sure what! Try refreshing the page and trying again.",
                })
              }
              return done(null, {
                user: newUser,
              })
            })
          }
        })
      },
    ),
  )
}
