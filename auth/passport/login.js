const bCrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const LocalStrategy = require('passport-local')

module.exports = (passport, User) => {
  passport.use(
    'local-login',
    // TODO: remove username and password (they might be defaults)
    new LocalStrategy(
      {
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true,
      },
      (req, username, password, done) => {
        const isValidPassword = (userpass, password) => {
          return bCrypt.compareSync(password, userpass)
        }

        User.findOne({
          where: {
            username,
          },
        })
          .then(user => {
            if (!user) {
              return done({
                message: 'Your provided email does not exist in our system.',
              })
            }

            if (!isValidPassword(user.password, password)) {
              return done({
                message: 'Your provided password is incorrect.',
              })
            }

            const userInfo = user.get()

            const token = jwt.sign(
              {
                id: userInfo.id,
                username: userInfo.username,
              },
              process.env.JWT_SECRET,
              {
                expiresIn: '1hr',
              },
            )

            return done(null, {
              token,
              user: userInfo,
            })
          })
          .catch(err => {
            console.log(`Error: ${err}`)

            return done({
              message:
                'Something went wrong with your sign in. Please try again.',
            })
          })
      },
    ),
  )
}
