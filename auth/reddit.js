const passport = require('passport')
const session = require('express-session')
const cookieParser = require('cookie-parser')
const methodOverride = require('method-override')
const jwt = require('jsonwebtoken')
const crypto = require('crypto')
const RedditStrategy = require('passport-reddit').Strategy

const User = require('./../models').User

const setupRedditAuth = app => {
  passport.serializeUser((user, done) => {
    done(null, user)
  })

  passport.deserializeUser((obj, done) => {
    done(null, obj)
  })

  passport.use(
    new RedditStrategy(
      {
        clientID: process.env.REDDIT_CONSUMER_KEY,
        clientSecret: process.env.REDDIT_CONSUMER_SECRET,
        callbackURL: 'http://localhost:9090/auth/reddit/callback',
      },
      (accessToken, refreshToken, profile, done) => {
        User.findOrCreate({
          where: {
            username: profile.name,
          },
        }).spread((user, created) => {
          const token = jwt.sign(
            { id: user.id, name: profile.name },
            process.env.JWT_SECRET,
            {
              expiresIn: '1hr', // expire in one hour
              // expiresIn: 60 // expire in one minute
            },
          )

          done(null, token)
        })
      },
    ),
  )

  app.use(cookieParser('glovecat'))
  app.use(methodOverride())
  app.use(
    session({
      secret: 'keyboard cat',
      saveUninitialized: true,
      resave: false,
      cookie: {
        maxAge: 1000 * 60 * 60,
        secure: false,
        httpOnly: false,
      },
    }),
  )
  app.use(passport.initialize())
  app.use(passport.session())
}

const authWithReddit = (req, res, next) => {
  req.session.state = crypto.randomBytes(32).toString('hex')
  passport.authenticate('reddit', {
    state: req.session.state,
  })(req, res, next)
}

const validateRedditAuth = (req, res, next) => {
  if (req.query.state == req.session.state) {
    passport.authenticate('reddit', function(n, token) {
      res.cookie('token', token, { maxAge: 1000 * 60 * 60 })
      res.redirect('http://localhost:3000')
    })(req, res, next)
  } else {
    next(new Error(403))
  }
}

module.exports = {
  authenticate: authWithReddit,
  setup: setupRedditAuth,
  validate: validateRedditAuth,
}
