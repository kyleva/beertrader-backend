const routes = require('express').Router()
const passport = require('passport')
const createPost = require('./../queries/post/create-post')
const getPost = require('./../queries/post/get-post')
const getAllPosts = require('./../queries/post/get-all-posts')

const createComment = require('./../queries/comment/create-comment')

// const createUser = require('./../queries/user/create-user')
const getUser = require('./../queries/user/get-user')
const logoutUser = require('./../auth/passport/logout')
// const loginUser = require('./../queries/user/login-user')

routes.post('/post', createPost)
routes.post('/post/:postId', getPost)
routes.get('/posts', getAllPosts)

routes.post('/comment', createComment)

// refactor these login routes to be in their separate places
routes.post('/user/create', function(req, res, next) {
  passport.authenticate('local-register', (error, data) => {
    if (error) {
      res.status(200).json({
        isError: true,
        message: error.message,
      })
      return
    }

    res.status(200).json({
      user: data.user,
    })
  })(req, res, next)
})

routes.post('/user/login', function(req, res, next) {
  passport.authenticate('local-login', (error, { token } = {}) => {
    console.log(error)
    if (error) {
      res.status(200).json({
        isError: true,
        message: error.message,
      })
      return
    }

    /**
     * consider sending token
     */
    res.status(200).json({
      success: true,
      token,
    })

    next()
  })(req, res, next)
})

routes.get('/user/:username', getUser)

module.exports = routes
